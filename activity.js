db.users.deleteMany(
	{ 
		"_id": { $in: 
			[	ObjectId("621f268b63088f19ef61917b"),
				ObjectId("621f2c2463088f19ef61917e"),
				ObjectId("621f2c5b63088f19ef61917f"),
				ObjectId("621f2c5b63088f19ef619180")
			]
			}
	}
)

db.users.insertMany(
		[
		{
	    "firstName" : "Jane",
	    "lastName" : "Doe",
	    "age" : 21.0,
	    "contact" : {
	        "phone" : "87654321",
	        "email" : "janedoe@gmail.com"
	    },
	    "courses" : [ 
	        "CSS", 
	        "Javascript", 
	        "Python"
	    ],
	    "department" : "HR",
	    "isAdmin" : false,
	    "status" : "active"
		},

		{
	    "firstName" : "Stephen",
	    "lastName" : "Hawking",
	    "age" : 76.0,
	    "contact" : {
	        "phone" : "87654321",
	        "email" : "stephenhawking@gmail.com"
	    },
	    "courses" : [ 
	        "Python", 
	        "React", 
	        "PHP"
	    ],
	    "department" : "HR",
	    "status" : "active"
		},

		{
	    "firstName" : "Neil",
	    "lastName" : "Armstrong",
	    "age" : 82.0,
	    "contact" : {
	        "phone" : "87654321",
	        "email" : "neilarmstrong@gmail.com"
	    },
	    "courses" : [ 
	        "React", 
	        "Laravel", 
	        "Sass"
	    ],
	    "department" : "HR",
	    "status" : "active"
		},

		{
	    "firstName" : "Bill",
	    "lastName" : "Gates",
	    "age" : 65.0,
	    "contact" : {
	        "phone" : "12345678",
	        "email" : "bill@gmail.com"
	    },
	    "courses" : [ 
	        "PHP", 
	        "Laravel", 
	        "HTML"
	    ],
	    "department" : "Operations",
	    "status" : "active"
		}
		]

	)


db.users.find(
	{$or:[
		{firstName:{ $in: [ /^S/i ] }},
		{lastName:{ $in: [ /^D/i ] }}
		]},
		{
			_id: 0,
			age: 0,
			contact: 0,
			courses: 0,
			department: 0,
			status: 0,
			isAdmin: 0
		}
	
	)

db.users.find(
	{ $and: [ 
		{ firstName: "Neil" }, { age: {$gte:82} } ] }
	)

db.users.find(
	{$and: [
		{department: "HR"}, {age: {$gte:70}}]}
	)

db.users.find(
	{
		firstName: { $regex: "jane", $options:'i'}
	}
	)


db.users.find(
	{ $and: [ 
		{ firstName:{ $regex: "e", $options:'i' } }, 
		{ age: {$lte:30} } 
	] },
	{ isAdmin: 0, status:0}
	)